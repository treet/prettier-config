# @treet/prettier-config

[![npm version](https://badge.fury.io/js/%40treet%2Fprettier-config.svg)](https://badge.fury.io/js/%40treet%2Fprettier-config.svg)

Shared prettier configuration.
